#!/usr/bin/env bash

set -Eeuo pipefail

CERT_DIR="/opt/postfix/ssl"

if [[ -f "$CERT_DIR/tls.crt" ]]; then
  CERT_FILE="$CERT_DIR/tls.crt"
elif [[ -f "$CERT_DIR/tls.pem" ]]; then
  CERT_FILE="$CERT_DIR/tls.pem"
else
  exit 0
fi

update-postfix-config.sh <(
  echo "smtpd_tls_cert_file = $CERT_FILE"
  echo "smtpd_tls_key_file = \$smtpd_tls_cert_file"
  echo "smtpd_tls_CAfile = \$smtpd_tls_cert_file"
)

if [[ -f "$CERT_DIR/tls.key" ]]; then
  update-postfix-config.sh <(echo "smtpd_tls_key_file = $CERT_DIR/tls.key")
fi

if [[ -f "$CERT_DIR/ca.crt" ]]; then
  update-postfix-config.sh <(echo "smtpd_tls_CAfile = $CERT_DIR/ca.crt")
elif [[ -f "$CERT_DIR/ca.pem" ]]; then
  update-postfix-config.sh <(echo "smtpd_tls_CAfile = $CERT_DIR/ca.pem")
fi
