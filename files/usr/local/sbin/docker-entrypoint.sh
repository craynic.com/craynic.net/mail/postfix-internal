#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d"

ENVS=(
  POSTFIX_HOSTNAME
  POSTFIX_RELAY_TRANSPORT
  POSTFIX_SMTPD_MILTERS
  POSTFIX_MESSAGE_SIZE_LIMIT
)

# cleanup ENV variables
for e in "${ENVS[@]}"; do
  unset "$e"
done

# run
exec "$@"
