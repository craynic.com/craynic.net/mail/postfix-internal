#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_RELAY_TRANSPORT" ]]; then
  echo "Missing nexthop address for relay_transport." >/dev/stderr
  exit 1
fi
